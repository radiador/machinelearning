//
//  ViewController.swift
//  Machine Learning App
//
//  Created by formador on 22/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import CoreML
import Vision

class ViewController: UIViewController {

    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var infoButtonItem: UIBarButtonItem!
    
    private var model: VNCoreMLModel?
    private var requestDetectImage: VNCoreMLRequest?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        infoButtonItem.title = "Wainting scene..."
        
        if let model = try? VNCoreMLModel(for: GoogLeNetPlaces().model) {
            
            requestDetectImage = VNCoreMLRequest(model: model) { [weak self] request, error in
                guard let results = request.results as? [VNClassificationObservation],
                    let topResult = results.first else {
                        fatalError("unexpected result type from VNCoreMLRequest")
                }
                
                DispatchQueue.main.async { [weak self] in
                    self?.infoButtonItem.title = "\(Int(topResult.confidence * 100))% it's \(topResult.identifier)"
                }
            }
        }
        
    }
    

    @IBAction func toolbarButtonActionPressed(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.savedPhotosAlbum) {
            
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.sourceType = .savedPhotosAlbum
            imagePickerController.delegate = self
            
            present(imagePickerController, animated: true, completion: nil)
            
        }
    }
}

extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let imageSelected = info[.originalImage] as? UIImage {
            
            photoImageView.image = imageSelected
            
            guard let ciImage = CIImage(image: imageSelected) else {
                fatalError("couldn't convert UIImage to CIImage")
            }
            
            detectScene(image: ciImage)
        }
        
        dismiss(animated: true, completion: nil)
    }
}

// MARK: - Methods
extension ViewController {
    
    func detectScene(image: CIImage) {
        infoButtonItem.title = "detecting scene..."
        
        // Load the ML model through its generated class
        guard let requestDetectImage = requestDetectImage else { return }
        
        // Run the Core ML GoogLeNetPlaces classifier on global dispatch queue
        let handler = VNImageRequestHandler(ciImage: image)
        DispatchQueue.global(qos: .userInteractive).async {
            do {
                try handler.perform([requestDetectImage])
            } catch {
                print(error)
            }
        }
    }
}


